module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160262/learn_bootstrap/'
    : '/'
}
